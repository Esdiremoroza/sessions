// arguments and parameters

function printName(name){
	console.log("Im the real "+name);
}

printName("Slim Shady");

function checkDivisibilityBy2(number){
	let result = number % 2;
	console.log("The remainder of "+number+" is " +result);
}
checkDivisibilityBy2(10);

// Multiple Arguments and Parameters

function createFullName(firstName,middleName,lastName){
	console.log(firstName+" "+middleName+" "+lastName);
}
createFullName("Juan","Dela","Cruz");

// Usage of Prompts and Alerts
let user_name = prompt("Enter your username");

function displayWelcomMessageForUser(userName){
	alert("Welcome back to Valorant "+userName);
}
displayWelcomMessageForUser(user_name);