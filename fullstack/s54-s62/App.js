import './App.css';
import AppNavbar from './components/AppNavBar.js';
import {Container} from 'react-bootstrap';
import Home from './pages/Home.js';
import Courses from './pages/Courses.js';

function App() {
  return (
    <>
      <AppNavbar/>
      <Container>
        <Home/>
        <Courses/>
      </Container>   
    </>
  );
}

export default App;
